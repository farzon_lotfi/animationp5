#include "MyWorld.h"
#include <iostream>

using namespace Eigen;
using namespace dart::dynamics;

MyWorld::MyWorld() {
  // Load a skeleton from file
  mSkel = dart::utils::SkelParser::readSkeleton(DART_DATA_PATH"skel/human.skel");

  // Create markers
  createMarkers();
    std::vector<Eigen::Vector3d> initVec(mMarkers.size(),Vector3d::Zero());
    mTarget = initVec; //hard copy
    mC = initVec; //hard copy
    //mConstrainedMarkers = std::vector<int>(mMarkers.size(),-1);
  // Initialize Jacobian assuming that there is only one constraint
  for (int i = 0; i < mMarkers.size(); i++) {
      mJ.push_back(MatrixXd::Zero(3, mSkel->getNumDofs()));
  }
}

MyWorld::~MyWorld() {
}

void MyWorld::solve() {
    int numIter = 300;
    double alpha = 0.01;
    int nDof = mSkel->getNumDofs();
    for (int i = 0; i < mMarkers.size(); i++) {
        if (mConstrainedMarkers.find(i) != mConstrainedMarkers.end()) {
            VectorXd gradients(nDof); // reset at the start of tracked marker
            VectorXd newPose(nDof);
            for (int j = 0; j < numIter; j++) {
                //hmm no indexing needed
                gradients = updateGradients();
                newPose = mSkel->getPositions() - alpha * gradients;
                mSkel->setPositions(newPose);
                mSkel->computeForwardKinematics(true, false, false); // DART updates all the transformations based on newPose
            }
        }
  }
  
}

// Current code only works for the left leg with only one constraint
VectorXd MyWorld::updateGradients() {
  VectorXd gradients = VectorXd::Zero(mSkel->getNumDofs());
  Vector4d offset;
  for (int i = 0; i < mMarkers.size(); i++) {
    if (mConstrainedMarkers.find(i) == mConstrainedMarkers.end()) {
      continue;
    }
    // compute c(q)
    mC[i] = getMarker(i)->getWorldPosition() - mTarget[i];

    // compute J(q)
    offset << getMarker(i)->getLocalPosition(), 1; // Create a vector in homogeneous coordinates
    BodyNode *node = getMarker(i)->getBodyNode();
    BodyNode *root = mSkel->getRootBodyNode();
    //while (node->getParentBodyNode()) { // maybe 1 off error?
    while (node != root) {
      Joint *joint = node->getParentJoint();
      //How to get the number of dofs in a joint, jt?
      int nDofs = joint->getNumDofs();
      Vector4d jCol(0,0,0,0);
      Matrix4d jointToChild = joint->getTransformFromChildBodyNode().inverse().matrix();
      // Jacobian implementation step 1
      Matrix4d worldToParent = node->getParentBodyNode()->getTransform().matrix();
      // step 2
      Matrix4d parentToJoint = joint->getTransformFromParentBodyNode().matrix();
      Matrix4d transformAccum = parentToJoint;
      for(int j = 0; j < nDofs; j++) {
        // step 4 (doing this a little early b\c need to mat multpy dR when k==j)
        Matrix4d dR = joint->getTransformDerivative(j);
        Matrix4d Rn;
        Matrix4d Rn1 = Matrix4d::Identity();
        Matrix4d Rn2 = Matrix4d::Identity(); // may be easier to split in half
        //updates all
        /*for(int k = 0; k < nDofs; k++){
          if(k == j) {
            Rn *= dR;
          }
          // step 3
          Rn *= joint->getTransform(k).matrix();
        }*/
        for (int k = 0; k < j; k++) {
          // step 3
          Rn1 *= joint->getTransform(k).matrix();
        }
        for (int k = j+1; k < nDofs; k++) {
          // step 3
          Rn2 *= joint->getTransform(k).matrix();
        }
        Rn = Rn1 * dR *Rn2;
        jCol = worldToParent * parentToJoint * Rn * jointToChild * offset;

        int colIndex = joint->getIndexInSkeleton(j);
        mJ[i].col(colIndex) = jCol.head(3);
        transformAccum *= joint->getTransform(j).matrix();
      }
      //update offset
       offset = transformAccum * jointToChild * offset;
      // increment
      node = node->getParentBodyNode();
    }
    gradients += 2 * mJ[i].transpose() * mC[i];
  }
    return gradients;
}

// Current code only handlse one constraint on the left foot.
void MyWorld::createConstraint(int _index) {
  if (mConstrainedMarkers.find(_index) == mConstrainedMarkers.end()) {
    mTarget[_index] = getMarker(_index)->getWorldPosition();
    mConstrainedMarkers.insert(_index);
  }
}

void MyWorld::modifyConstraint(int _index, Vector3d _deltaP) {
  if (mConstrainedMarkers.find(_index) != mConstrainedMarkers.end()) {
    mTarget[_index] += _deltaP;
  }
}

void MyWorld::removeConstraint(int _index) {
  if (mConstrainedMarkers.find(_index) != mConstrainedMarkers.end()) {
    mConstrainedMarkers.erase(_index);
    mTarget[_index] = Vector3d::Zero();
  }
}

Marker* MyWorld::getMarker(int _index) {
  return mMarkers[_index];
}

void MyWorld::createMarkers() {
  Vector3d offset(0.2, 0.0, 0.0);
  BodyNode* bNode = mSkel->getBodyNode("h_heel_right");
  Marker* m = new Marker("right_foot", offset, bNode);
  mMarkers.push_back(m);
  bNode->addMarker(m);

  offset = Vector3d(0.2, 0.0, 0.0);
  bNode = mSkel->getBodyNode("h_heel_left");
  m = new Marker("left_foot", offset, bNode);
  mMarkers.push_back(m);
  bNode->addMarker(m);

  offset = Vector3d(0.065, -0.3, 0.0);
  bNode = mSkel->getBodyNode("h_thigh_right");
  m = new Marker("right_thigh", offset, bNode);
  mMarkers.push_back(m);
  bNode->addMarker(m);

  offset = Vector3d(0.065, -0.3, 0.0);
  bNode = mSkel->getBodyNode("h_thigh_left");
  m = new Marker("left_thigh", offset, bNode);
  mMarkers.push_back(m);
  bNode->addMarker(m);

  offset = Vector3d(0.0, 0.0, 0.13);
  bNode = mSkel->getBodyNode("h_pelvis");
  m = new Marker("pelvis_right", offset, bNode);
  mMarkers.push_back(m);
  bNode->addMarker(m);

  offset = Vector3d(0.0, 0.0, -0.13);
  bNode = mSkel->getBodyNode("h_pelvis");
  m = new Marker("pelvis_left", offset, bNode);
  mMarkers.push_back(m);
  bNode->addMarker(m);

  offset = Vector3d(0.075, 0.1, 0.0);
  bNode = mSkel->getBodyNode("h_abdomen");
  m = new Marker("abdomen", offset, bNode);
  mMarkers.push_back(m);
  bNode->addMarker(m);

  offset = Vector3d(0.0, 0.18, 0.075);
  bNode = mSkel->getBodyNode("h_head");
  m = new Marker("head_right", offset, bNode);
  mMarkers.push_back(m);
  bNode->addMarker(m);

  offset = Vector3d(0.0, 0.18, -0.075);
  bNode = mSkel->getBodyNode("h_head");
  m = new Marker("head_left", offset, bNode);
  mMarkers.push_back(m);
  bNode->addMarker(m);

  offset = Vector3d(0.0, 0.22, 0.0);
  bNode = mSkel->getBodyNode("h_scapula_right");
  m = new Marker("right_scapula", offset, bNode);
  mMarkers.push_back(m);
  bNode->addMarker(m);

  offset = Vector3d(0.0, 0.22, 0.0);
  bNode = mSkel->getBodyNode("h_scapula_left");
  m = new Marker("left_scapula", offset, bNode);
  mMarkers.push_back(m);
  bNode->addMarker(m);

  offset = Vector3d(0.0, -0.2, 0.05);
  bNode = mSkel->getBodyNode("h_bicep_right");
  m = new Marker("right_bicep", offset, bNode);
  mMarkers.push_back(m);
  bNode->addMarker(m);

  offset = Vector3d(0.0, -0.2, -0.05);
  bNode = mSkel->getBodyNode("h_bicep_left");
  m = new Marker("left_bicep", offset, bNode);
  mMarkers.push_back(m);
  bNode->addMarker(m);

  offset = Vector3d(0.0, -0.1, 0.025);
  bNode = mSkel->getBodyNode("h_hand_right");
  m = new Marker("right_hand", offset, bNode);
  mMarkers.push_back(m);
  bNode->addMarker(m);

  offset = Vector3d(0.0, -0.1, -0.025);
  bNode = mSkel->getBodyNode("h_hand_left");
  m = new Marker("left_hand", offset, bNode);
  mMarkers.push_back(m);
  bNode->addMarker(m);
}
